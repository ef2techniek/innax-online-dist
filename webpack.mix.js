const mix = require('laravel-mix');

mix.options({
    processCssUrls: false
});

mix.js('src/js/auth.js', 'dist/js')
    .js('src/js/app.js', 'dist/js');

mix.sass('src/sass/auth.scss', 'dist/css/auth.css')
    .sass('src/sass/app.scss', 'dist/css/app.css');

mix.copy('./node_modules/bootstrap.native/dist/bootstrap-native-v4.min.js', 'dist/js/bootstrap-native-v4.min.js');
