export const customChartLegend = {
    data() {
        return {
            options: {
                legend: {
                    display: false,
                },
                legendCallback: function(chart) {
                    var legendHtml = [];
                    legendHtml.push('<ul>');
                    var item = chart.data.datasets[0];
                    var sum = item.data.reduce(function add(a, b) { return a + b; }, 0);
                    for (var i=0; i < item.data.length; i++) {
                        var perc = Math.round(100*item.data[i]/sum,0);
                        legendHtml.push('<li>');
                        legendHtml.push('<span class="chart-legend" style="background-color:' + item.backgroundColor[i] +'"></span>');
                        legendHtml.push('<span class="chart-legend-label-text">'+chart.data.labels[i]+'</span>');
                        legendHtml.push('<span class="chart-percentage">'+ perc +'%</span>');
                        legendHtml.push('</li>');
                    }

                    legendHtml.push('</ul>');
                    return legendHtml.join("");
                }
            }
        }
    },
};