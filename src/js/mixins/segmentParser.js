export default {
    methods: {
        segmentParser: function(segments, key = 'text', value = 'value') {
            let parsedData = {};
            let chartSegments = _.sortBy(segments, key);

            _.each(chartSegments, (chartSegment) => {
                parsedData[chartSegment[key]] = chartSegment[value];
            });

            return parsedData;
        }
    }
}