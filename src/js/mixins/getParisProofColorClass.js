export default {
    methods: {
        calculateParisProofScore(consumption, norm) {
            return Math.ceil((consumption * 100) / norm);
        },

        getParisProofColorClass(parisProofScore) {
            if (parisProofScore <= 100) {
                return 'color-green';
            }

            if (parisProofScore >= 100 && parisProofScore <= 200) {
                return 'color-orange';
            }

            return 'color-red';
        }
    }
};
