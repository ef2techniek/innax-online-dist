export const genericFilterItem = {
    props: {
        name: String,
        fieldName: String,
        filterSetActiveFilter: ''
    },

    data() {
        return {
            active: false,
            expanded: false
        }
    },

    computed: {
        nameLowerCase: function() {
            return _.toLower(this.name);
        }
    },

    methods: {
        toggle: function () {
            this.expanded = !this.expanded;
        },

        collapse: function() {
            this.expanded = false;
        }
    },

    watch: {
        expanded: function(isExpanded) {
            if (isExpanded === false) {
                return;
            }

            this.$emit('filter-expanded', { name: this.name });
        },

        filterSetActiveFilter: function(activeFilterName) {
            if (activeFilterName !== this.name) {
                this.collapse();
            }
        }
    }
};
