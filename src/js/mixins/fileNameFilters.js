export const fileNameFilters = {
    filters: {
        stripExtension: function(file) {
            if (_.isNil(file) === true || _.isString(file) === false) {
                return '';
            }

            let splittedFileString = file.split('.');

            if (splittedFileString.length > 1) {
                return splittedFileString.slice(0, -1).join('.');
            }

            return file;
        },

        getExtension: function(file) {
            if (_.isNil(file) === true || _.isString(file) === false) {
                return '';
            }

            let splittedFileString = file.split('.');

            if(splittedFileString.length === 0) {
                return '';
            }

            return '.'+splittedFileString.pop();
        }
    }
};
