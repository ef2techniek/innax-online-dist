import parisProofColorClassMixin from './getParisProofColorClass';
import FilterSet from "../components/app/FilterSet";

export default {
    components: {FilterSet},

    mixins: [parisProofColorClassMixin],

    props: {
        apiUrl: {
            type: String,
            required: true
        },

        autocompleteApiUrl: {
            type: String,
            required: true
        }
    },

    data() {
        return {
            gridData: [],
            isLoading: false,
            infiniteScrollActive: false,
            fetchDataOrigin: 'initial-load',
            currentPage: 1,
            totalPages: null,
            filters: [],
            filterData: {},
            chartData: {}
        }
    },

    mounted() {
        this.fetchData();
    },

    methods: {
        fetchData: function () {
            this.isLoading = true;

            axios.post(this.buildApiUrl(), this.prepareFilterData())
                .then(response => {
                    if (this.validateResponse(response.data) === false) {
                        throw "Invalid response";
                    }

                    if (this.currentPage === 1) {
                        this.$set(this, 'gridData', [...response.data.data]);
                    } else {
                        this.$set(this, 'gridData', [...this.gridData, ...response.data.data]);
                    }

                    this.parseChartData(response.data);

                    this.currentPage++;
                    this.infiniteScrollActive = response.data.current_page < response.data.last_page;
                })
                .catch(error => {
                    this.$toaster.error('Ophalen van de gegevens is mislukt');
                })
                .finally(() => {
                    this.isLoading = false;
                });
        },

        buildApiUrl: function () {
            let params = new URLSearchParams({
                page: this.currentPage.toString(),
                'event-origin': this.fetchDataOrigin
            });

            if (this.apiUrl.includes('?')) {
                return `${this.apiUrl}&${params.toString()}`;
            }

            return `${this.apiUrl}?${params.toString()}`;
        },

        prepareFilterData: function() {
            return {...this.filterData};
        },

        validateResponse: function (response) {
            if (_.isNil(response.data)) {
                return false;
            }

            if (_.isNil(response.last_page) || _.isNumber(response.last_page) === false) {
                return false;
            }

            if (_.isNil(response.current_page) || _.isNumber(response.current_page) === false) {
                return false;
            }

            return true;
        },

        handleLoaderVisibility: function (payload) {
            if (payload.visible === true && this.isLoading === false) {
                this.fetchDataOrigin = 'infinite-scroll';
                this.fetchData();
            }
        },

        handleFiltersChanged: function (payload) {
            this.currentPage = 1;
            this.fetchDataOrigin = 'filter-changed';

            let filters = {};

            _.each(payload, filterData => {
                filters[_.toLower(filterData.name)] = {...filterData};
                delete filters[_.toLower(filterData.name)].name;
            })

            this.$set(this, 'filterData', filters);
            this.fetchData();
        },

        parseChartData: function (sourceData) {
            return [];
        }
    }
}