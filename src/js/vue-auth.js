window.Vue = require('vue');

/**
 * Event hub
 */
window.eventHub = new Vue();

/**
 * Components
 */
Vue.component('repeatable-building-fieldset', require('./components/auth/RepeatableBuildingFieldset.vue').default);
Vue.component('password-toggler', require('./components/auth/PasswordToggler.vue').default);

/**
 * Main
 */
new Vue({
    'el': '#auth-root'
});
