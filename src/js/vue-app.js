window.Vue = require('vue');

/**
 * Event hub
 */
window.eventHub = new Vue();

/**
 * Vendor
 */
import PerfectScrollbar from 'vue2-perfect-scrollbar'
import '../../node_modules/vue2-perfect-scrollbar/dist/vue2-perfect-scrollbar.css'
Vue.use(PerfectScrollbar, { options: { 'wheelPropagation': false } });

import vue2Dropzone from 'vue2-dropzone'
import 'vue2-dropzone/dist/vue2Dropzone.min.css'
Vue.component('vueDropzone', vue2Dropzone);

import Toaster from 'v-toaster'
import 'v-toaster/dist/v-toaster.css'
Vue.use(Toaster, {timeout: 5000});

import VueNumber from 'vue-number-animation'
Vue.use(VueNumber)


/**
 * General
 */
Vue.component('dashboard', require('./components/app/Dashboard').default);
Vue.component('information-dropdown', require('./components/app/InformationDropdown').default);
Vue.component('information-modal', require('./components/app/InformationModal').default);
Vue.component('paris-proof-score', require('./components/app/ParisProofScore').default);
Vue.component('eiffel-tower', require('./components/app/EiffelTower').default);
Vue.component('alert', require('./components/app/Alert').default);
Vue.component('year-slider', require('./components/app/YearSlider').default);
Vue.component('loader', require('./components/app/Loader').default);
Vue.component('filter-set', require('./components/app/FilterSet').default);
Vue.component('search-filter-item', require('./components/app/filters/SearchFilterItem').default);
Vue.component('list-filter-item', require('./components/app/filters/ListFilterItem').default);
Vue.component('range-filter-item', require('./components/app/filters/RangeFilterItem').default);
Vue.component('tabs', require('./components/app/tabs/Tabs').default);
Vue.component('tab', require('./components/app/tabs/Tab').default);
Vue.component('search', require('./components/app/Search').default);
Vue.component('table-expiration-date-age', require('./components/app/TableExpirationDateAge').default);
Vue.component('energy-label', require('./components/app/EnergyLabel').default);

/**
 * Charts
 */
Vue.component('simple-doughnut-chart', require('./components/app/charts/SimpleDoughnutChart').default);
Vue.component('advanced-doughnut-chart', require('./components/app/charts/AdvancedDoughnutChart').default);
Vue.component('percentage-chart', require('./components/app/charts/PercentageChart').default);
Vue.component('chart-measures', require('./components/app/charts/ChartMeasures').default);
Vue.component('advanced-horizontal-bar-chart', require('./components/app/charts/AdvancedHorizontalBarChart').default);
Vue.component('chart-investment-vs-savings', require('./components/app/charts/ChartInvestmentVsSavings').default);
Vue.component('advanced-line-chart', require('./components/app/charts/AdvancedLineChart').default);
Vue.component('chart-square-meters', require('./components/app/charts/ChartSquareMeters').default);
Vue.component('chart-building-functions', require('./components/app/charts/ChartBuildingFunctions').default);
Vue.component('chart-building-energy-labels', require('./components/app/charts/ChartBuildingEnergyLabels').default);
Vue.component('chart-measure-consumption', require('./components/app/charts/ChartMeasureConsumption').default);

/**
 * Buildings
 */
Vue.component('building-overview', require('./components/app/buildings/BuildingOverview').default);
Vue.component('add-building-modal', require('./components/app/buildings/AddBuildingModal').default);

/**
 * Files
 */
Vue.component('files-overview', require('./components/app/files/FilesOverview').default);
Vue.component('file-browser', require('./components/app/files/FileBrowser').default);
Vue.component('directories', require('./components/app/files/Directories').default);
Vue.component('favorite-files', require('./components/app/files/FavoriteFiles').default);
Vue.component('toggle-favorite-file', require('./components/app/files/ToggleFavoriteFile').default);
Vue.component('recent-files', require('./components/app/files/RecentFiles').default);
Vue.component('file-upload-modal', require('./components/app/files/FileUploadModal').default);
Vue.component('uploaded-file', require('./components/app/files/UploadedFile').default);
Vue.component('uploaded-file-meta', require('./components/app/files/UploadedFileMeta').default);

/**
 * Measures
 */
Vue.component('measure-overview', require('./components/app/measures/MeasureOverview').default);
Vue.component('savings-formula', require('./components/app/measures/SavingsFormula').default);
Vue.component('log-modal', require('./components/app/measures/LogModal').default);
Vue.component('add-measure-modal', require('./components/app/measures/AddMeasureModal').default);

/**
 * Main
 */
new Vue({
    'el': '#app-root'
});
